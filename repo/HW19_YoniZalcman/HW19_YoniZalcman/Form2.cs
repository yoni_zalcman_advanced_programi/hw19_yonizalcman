﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace HW19_YoniZalcman
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }
        string username;
        public string userNameIn
        {
            get { return username; }
            set { username = value; }
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            string line;
            string date;
            string name;
            using (StreamReader sr = new StreamReader(username + "BD.txt"))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    date = line.Split(',').Last();
                    name = line.Split(',').First();
                    if (monthCalendar1.SelectionRange.Start.ToString() == date)
                    {
                        label1.Text = name + "'s Birthday Mazal Tov";
                    }
                    else
                    {
                        label1.Text = "No one has a Birthday today";
                    }
                }
            }
        }
    }
}
