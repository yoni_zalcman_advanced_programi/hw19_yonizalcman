﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace HW19_YoniZalcman
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Open the text file using a stream reader.
            using (StreamReader sr = new StreamReader("Users.txt"))
            {
                // Read the stream to a string, and write the string to the console.
                String line;
                var worked=0;
                var form=new Form2();
                while ((line = sr.ReadLine()) != null)
                {
                    if(line==textBox1.Text+','+textBox2.Text)
                    {
                        worked=1;
                        form.Show();
                        form.userNameIn = UserNameBox;
                        this.Visible = false;
                    }
                }
                if(worked!=1)
                {
                    MessageBox.Show("Wrong User name or Password");
                }
                sr.Close();
            }
        }

        public string UserNameBox
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }
    }
}
